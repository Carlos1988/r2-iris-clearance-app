var webpack = require('webpack')
var path =  require('path')
var helpers = require('./config/helpers')

var copyWebpackPlugin = require('copy-webpack-plugin')
var cleanWebpackPlugin = require('clean-webpack-plugin')
var extractTextPlugin = require("extract-text-webpack-plugin");
var htmlWebpackPlugin = require('html-webpack-plugin')

var publishFiles = require('./publish-files.json')

module.exports = {
  entry: {
    app: './src/app/modules/main/main.module.js',
    dashboard_app: './src/app/modules/dashboard/dashboard.module.js',
    vendor: [
      'angular',
      'angular-animate',
      'angular-sanitize',
      'angular-ui-bootstrap',
      'angular-ui-router',
      'oclazyload',
      './node_modules/bootstrap-css-only/css/bootstrap.min.css',
      './node_modules/bootstrap-css-only/css/bootstrap-theme.min.css',
      './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css'
    ]
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'js/compress.[name].min.js'
  },
  resolve: {
    modules: ['node_modules'],
    extensions: ['.js']
  },
  module: {
    loaders: [
      { test: /\.html$/, use: [ {loader: 'html-loader', options: { minimize: true, removeComments: false, collapseWhitespace:false } }] },
      { test: /\.css$/, loader: extractTextPlugin.extract({use: "css-loader" }) },
      { test: /\.jpg$/, loader: "file-loader" },
      { test: /\.png$/, loader: "url-loader?mimetype=image/png"},
      { test: /\.gif$/, loader: "url-loader?mimetype=image/gif"},
      { test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/font-woff'},
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/octet-stream'},
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader'},
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=image/svg+xml'}
    ]
  },
  plugins: [
      new cleanWebpackPlugin(['public', 'logs']),
      // new webpack.optimize.CommonsChunkPlugin({
      //   name: "vendor",
      //   filename:"js/vendor.common.min.js"
      // }),
      new extractTextPlugin({ filename: 'css/[name].css', allChunks: true }),
      new webpack.HotModuleReplacementPlugin(),
      new copyWebpackPlugin(publishFiles),
      new htmlWebpackPlugin({
        title: 'Iris Police Clearance',
        template: helpers.root('src/assets/views', 'index.html'),
        chunks: ['vendor', 'app'],
        filename: 'views/index.html',
        inject: 'head'
      }),
      new webpack.NoEmitOnErrorsPlugin()
  ]
}
