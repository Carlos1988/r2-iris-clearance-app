var router = function($stateProvider){
  $stateProvider.state('dashboard', require('./routes/dashboard.route'))
}
router.$inject = ['$stateProvider']
module.exports = router;
