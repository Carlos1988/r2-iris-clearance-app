var externalServices = function ($provide) {
  $provide.value('ExtServices', window.environment);
  return $provide.value('appRootApiUrl', window.environment.appRootApiUrl);
  // $provide.value('lqa_service_url', window.ltk_environment.lqa_service_url);
  // $provide.value('redirect_url', window.ltk_environment.redirect_url);
  // return $provide.value('app_id', window.ltk_environment.app_id);
};
externalServices.$inject = ['$provide'];
module.exports = externalServices;
