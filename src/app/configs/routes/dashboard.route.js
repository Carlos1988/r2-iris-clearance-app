module.exports = {
  url: '/dashboard',
  templateUrl: '../views/dashboard.html',
  controller: 'DashboardCtrl as dCtrl',
  resolve: {
    lazyLoad: ['$ocLazyLoad', function ($ocLazyLoad) {
      return $ocLazyLoad.load({
        name: 'dashboard',
        files: ['../js/compress.dashboard_app.min.js']
      });
    }]
  }
}
