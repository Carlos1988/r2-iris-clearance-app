var personFactory = function($http, appRootApiUrl) {
  var self = this
  self.list = function(page, size) {
    return $http.get(appRootApiUrl + '/personsofinterest?page=' + page + '&size=' + size)
    .then(function(resp){
      return resp.data
    })
  }

  return {
        list: self.list
    }
}

personFactory.$inject = ['$http', 'appRootApiUrl', 'NotifierFactory']

module.exports = personFactory
