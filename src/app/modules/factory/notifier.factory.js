var notifierFactory = function($rootScope) {
  var self = this
  if(!$rootScope.notifications) {
    $rootScope.notifications = []
  }

  $rootScope.close = function(index) {
    $rootScope.notifications.splice(index, 1);
  };

  self.post = function(type, message) {
      $rootScope.notifications.push({type: type, msg: message})
  }

  self.postPromise = function(promise, callback, successMessage, failMessage){
    promise.then(function(data){
      callback(data)
      if(successMessage) {
        self.post('success', successMessage)
      }
    }).catch(function(err){
      var message = undefined

      if(failMessage){
        message = failMessage
      } else if(err) {
        message = "Failed to process request."
      }
      
      if(message){
        self.post('danger', message)
      }
    })
  }

  return {
      post: self.post,
      postPromise: self.postPromise
  }
}

notifierFactory.$inject = ['$rootScope']

module.exports = notifierFactory
