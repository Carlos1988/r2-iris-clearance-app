angular.module('r2_iris_clearance_app', [
  'ui.bootstrap',
  'ui.router',
  'oc.lazyLoad'
])
.config(require('./../../configs/externalServices.config'))
.config(require('./../../configs/router.config'))
.factory('NotifierFactory', require('./../factory/notifier.factory'))
.controller('MainCtrl', require('./main.controller'))
