var dashboardController = function(POIFactory, NotifierFactory){
    var self = this
    self.personList
    self.listPerson = function(){
      var callback = function(data) {
        self.personList = data
      }
      NotifierFactory.postPromise(POIFactory.list(1, 10), callback)
    }

    self.listPerson()
}

dashboardController.$inject = ['POIFactory', 'NotifierFactory']

module.exports = dashboardController
