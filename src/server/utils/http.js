var logger = require('./logger');

module.exports = {
  handleHttpCallError: function (res) {
    return function (error) {
      var message = 'Unknown error.';
      var statusCode = 500;
      if (error.message) {
        logger.error("HTTP Call failed. Error: " + JSON.stringify(error));
        message = error.message;
        if(error.statusCode) {
          statusCode = error.statusCode;
        }
      } else if (error.error) {
        message = error.error.message;
        if(error.error.statusCode) {
          statusCode = error.error.statusCode;
        }
        logger.error("HTTP Call failed. Error: " + JSON.stringify(error.error));
      } else {
        logger.error("HTTP Call failed. Error: " + JSON.stringify(error));
      }
      if (message.indexOf("ECONNREFUSED") != -1) {
        message = "Failed to connect to server.";
      }
      res.status(statusCode).json({message: message});
    }
  }
};