module.exports = {
  /**
   * Formats the given timestamp to MM/DD/YYYY
   * @param timestamp - timestamp. Eg. 532915200000
   * @returns {string}
   */
  formatTimestamp: function (timestamp) {
    if (timestamp) {
      function padLeft(str) {
        var str = "" + str;
        var pad = "00";
        return pad.substring(0, pad.length - str.length) + str;
      }

      var date = new Date(timestamp);
      var month = padLeft(date.getMonth() + 1); // getMonth() is zero-based
      var day = padLeft(date.getDate());
      var year = date.getFullYear();
      return month + "/" + day + "/" + year;
    } else {
      return "";
    }
  }
};