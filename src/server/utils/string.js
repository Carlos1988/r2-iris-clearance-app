var isNotNullOrEmpty = function (string) {
  return string != null && typeof string === 'string' && string.trim().length > 0
};

module.exports = {
  isNotNullOrEmpty: isNotNullOrEmpty,
  isNullOrEmpty: function (string) {
    return !isNotNullOrEmpty(string);
  }
};