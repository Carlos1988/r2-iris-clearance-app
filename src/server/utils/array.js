module.exports = {
  getFirst: function (arr) {
    return Array.isArray(arr) && arr.length > 0 ? arr[0] : null
  }
};