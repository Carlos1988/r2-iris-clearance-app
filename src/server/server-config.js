var events = require('events');
var fs = require('fs-extra-promise');
var logger = require('./utils/logger');

var env = {
    timezone: 'Asia/Manila',
    root: '/iris/',
    port: 3002,
    appRootApiUrl: 'http://127.0.0.1:3002/iris/api',
    r2AuthApiUrl: 'http://127.0.0.1:3000/auth',
    r2IrisClearanceApiUrl: 'http://127.0.0.1:8083',
    r2IrisFileApi: 'http://127.0.0.1:8082'
  }

var config = {
  emitter: new events.EventEmitter(),
  env: env
}

config.load =  function () {
    var overrideFile = __dirname + '/app_config_overrides.json';
    fs.exists(overrideFile, function (exists) {
      if (exists) {
        fs.readFileAsync(overrideFile, 'utf8').then(function (contents) {
          json = JSON.parse(contents);
          var updateConfig = function (properties) {
            properties.forEach(function (property, index) {
              if (property in json) {
                config.env[property] = json[property]
              }
            });
          };
          updateConfig(['root', 'r2AuthApiUrl', 'r2IrisClearanceApiUrl', 'r2IrisFileApi']); // TODO: Make this dynamic
          config.emitter.emit('ready', config.env);
        }, function (error) {
          logger.warn('Error reading ' + overrideFile + '. Error: ' + error.message);
          config.emitter.emit('ready', config.env);
        });
      } else {
        logger.info('Using default configurations since ' + overrideFile + ' does not exists.');
        config.emitter.emit('ready', config.env);
      }
    });
  }

module.exports = config
