var express = require('express');
var app = express.Router({mergeParams: true});

var personsRoutes = require('./api/personsofinterest.route');

app.use('/personsofinterest', personsRoutes);

module.exports = app;
