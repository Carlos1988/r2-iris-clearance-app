module.exports = function (publicDir) {
  var fs = require('fs');
  var express = require('express');
  var router = express.Router();

  /* GET home page. */
  router.get('/', function (req, res, next) {
    res.render('index', {user: req.user});
  });

  router.get('*.html', function (req, res) {
    var url = req.url.split('?')[0];
    var filePath = publicDir + url;
    res.render(filePath);
  });

  return router;
};
