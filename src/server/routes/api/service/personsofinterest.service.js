var Promise = require('bluebird');
var rp = require('request-promise');
var env = require('./../../../server-config').env;
var stringUtil = require('./../../../utils/string');

module.exports = {
  save: function (body/*, userToken*/) {
    var hasMissingParameters = (stringUtil.isNullOrEmpty(body.firstName)
        && stringUtil.isNullOrEmpty(body.lastName)
        && stringUtil.isNullOrEmpty(body.alias))

    if (hasMissingParameters) {
      return Promise.resolve({
        errorMessage: 'Person of interest type and either firstname, lastname or alias must be set.'
      });
    } else {
      return rp.post(env.r2IrisClearanceApiUrl + '/personsofinterest', {
        json: true,
        body: body,
        // auth: {
        //   'bearer': userToken
        // }
      }).then(function (data) {
        return {
          personOfInterestId: data.id
        }
      });
    }
  },
  update: function (id, body, userToken) {
    if (stringUtil.isNullOrEmpty(body.type)
      || (
        stringUtil.isNullOrEmpty(body.firstName)
        && stringUtil.isNullOrEmpty(body.lastName)
        && stringUtil.isNullOrEmpty(body.alias)
      )
    ) {
      return Promise.resolve({
        errorMessage: 'Person of interest type and either firstname, lastname or alias must be set.'
      });
    } else {
      return rp.put(env.r2IrisClearanceApiUrl + '/personsofinterest/' + id, {
        json: true,
        body: body,
        // auth: {
        //   'bearer': userToken
        // }
      });
    }
  },
  show: function (data, token) {
    return rp.get(env.r2IrisClearanceApiUrl + '/personsofinterest', {
      json: true,
      qs: data,
      // auth: {
      //   'bearer': token
      // }
    })
  },

  index: function (id) {
    return rp.get(env.r2IrisClearanceApiUrl + '/personsofinterest/' + id, {
      json: true,
      // auth: {
      //   'bearer': req.user.token
      // }
    })
  },

  existsAll: function (data) {
    return rp.post(env.r2IrisClearanceApiUrl + '/personsofinterest/exists', {
      json: true,
      body: data,
      // auth: {
      //   'bearer': req.user.token
      // }
    })
  },

  exists: function (id) {
    return rp.get(env.r2IrisClearanceApiUrl + '/personsofinterest/' + id + '/exists', {
      json: true,
      // auth: {
      //   'bearer': req.user.token
      // }
    })
  },

  // Person of Interest Details
  details: function (id, data) {
    return rp.get(env.r2IrisClearanceApiUrl + '/personsofinterest/' + id + '/details', {
      json: true,
      qs: data,
      // auth: {
      //   'bearer': req.user.token
      // }
    })
  },

  saveDetails: function (id, data) {
    return rp.put(env.r2IrisClearanceApiUrl + '/personsofinterest/' + id + '/details', {
      json: true,
      body: data,
      // auth: {
      //   'bearer': req.user.token
      // }
    })
  }
}
