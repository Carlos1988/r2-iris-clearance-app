var express = require('express');
var router = express.Router({mergeParams: true});
var rp = require('request-promise');

var logger = require('./../../utils/logger');
var appConfig = require('./../../server-config');
var httpUtil = require('./../../utils/http');
var poiService = require('./service/personsofinterest.service');

router.get('/', function (req, res, next) {
  poiService.show(req.query)
  .then(function (data) {
    res.json(data);
  }).catch(httpUtil.handleHttpCallError(res));
});

router.get('/:id', function (req, res, next) {
  var id = req.params.id;
  poiService.index(id).then(function (person) {
    res.json(person);
  }).catch(httpUtil.handleHttpCallError(res));
});

router.post('/exists', function (req, res, next) {
  var body = req.body;
  poiService.existsAll(body).then(function (data) {
    res.json(data);
  }).catch(httpUtil.handleHttpCallError(res));
});

router.get('/:id/exists', function (req, res, next) {
  var id = req.params.id;
  poiService.exists(id).then(function (person) {
    res.json(person);
  }).catch(httpUtil.handleHttpCallError(res));
});

router.post('/', function (req, res, next) {
  poiService.save(req.body/*, req.user.token*/).then(function (result) {
    if (result.errorMessage) {
      res.status(400).json({
        message: result.errorMessage
      });
    } else {
      res.json(result);
    }
  }).catch(httpUtil.handleHttpCallError(res));
});

router.put('/:id', function (req, res, next) {
  var id = req.params.id;
  poiService.update(id, req.body, req.user.token).then(function (result) {
    if (result.errorMessage) {
      res.status(400).json({
        message: result.errorMessage
      });
    } else {
      res.json(result);
    }
  }).catch(httpUtil.handleHttpCallError(res));
});
// Person of Interest Details
router.get('/:id/details', function (req, res, next) {
  var id = req.params.id;
  poiService.details(id, data).then(function (data) {
    res.json(data);
  }).catch(httpUtil.handleHttpCallError(res));
});


router.post('/:id/details', function (req, res, next) {
  var id = req.params.id;
  var body = req.body;
  poiService.details(id, body).then(function (data) {
    res.json(data);
  }).catch(httpUtil.handleHttpCallError(res));
});

module.exports = router;
