var logger = require('./utils/logger')
var express = require('express')
var bodyParser = require('body-parser')
var cookieParser =  require('cookie-parser')
var path = require('path')
var ejs = require('ejs')
var app = express();

var appConfig = require('./server-config')
var apiRoutes = require('./routes/api')
var appRoutes = require('./routes/app')(publicDir)


var publicDir = path.join(__dirname + '/../../public')

logger.debug("Overriding 'Express' logger");
var isProduction = process.env.NODE_ENV === 'production'
app.use(require('morgan')(isProduction ? 'common' : 'dev', {'stream': logger.stream}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
//app.use(cookieParser);

app.use('/api', apiRoutes)

// view engine setup
// uncomment after placing your favicon in /public
//app.use(favicon(publicDir + 'favicon.ico'));
app.engine('html', ejs.renderFile);
app.set('views', publicDir + '/views');
app.set('view engine', 'html');

//require('./passport')(app);
app.use('/', appRoutes); // Route that would return views
// Should be after mounting of appRoutes to / since appRoutes would sometimes render html view templates


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
})

// development error handler
// will print stacktrace
if(!isProduction) {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

var base =  express();
base.use(appConfig.env.root, app)
base.use(express.static(publicDir))
module.exports = base
