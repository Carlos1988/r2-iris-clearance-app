// See https://github.com/scotch-io/easy-node-authentication
module.exports = function (app) {
  var logger = require('./utils/logger');
  var arrayUtil = require('./utils/array');
  var env = require('./app_config').env;
  var rp = require('request-promise');
  var passport = require('passport');
  var flash = require('connect-flash');
  var session = require('express-session');
  var LocalStrategy = require('passport-local').Strategy;

  // =========================================================================
  // passport session setup ==================================================
  // =========================================================================
  // required for persistent login sessions
  // passport needs ability to serialize and unserialize users out of session

  // used to serialize the user for the session
  passport.serializeUser(function (user, done) {
    done(null, user.token);
  });

  // used to deserialize the user
  passport.deserializeUser(function (token, done) {
    rp.get(env.r2AuthApiUrl + '/users/current', {
      json: true,
      auth: {
        'bearer': token
      }
    }).then(function (user) {
      if (user) {
        user.token = token
      }
      done(null, user);
    }, function (err) {
      logger.warn('Failed deserialize token ' + token + '. Error: ' + err.message);
      return done(null, false, {errorMessage: 'Invalid Token'});
    });
  });

  // =========================================================================
  // LOCAL LOGIN =============================================================
  // =========================================================================
  passport.use('local-login', new LocalStrategy({
      // by default, local strategy uses username and password, this will only show how to override default fields
      usernameField: 'username',
      passwordField: 'password',
      passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function (req, username, password, done) {
      // asynchronous
      process.nextTick(function () {
        rp.post(env.r2AuthApiUrl + '/token', {
          json: true,
          body: {
            username: username,
            password: password
          }
        }).then(function (result) {
          if (result.success) {
            done(null, {
              token: result.token,
              name: result.name,
              role: result.role
            });
          } else {
            logger.warn('Failed to login username [' + username + ']. Message: ' + result.message);
            req.flash('errorMessage', 'Invalid username or password');
            req.flash('username', username);
            return done(null, false);
          }
        }, function (err) {
          logger.error('Failed to get token for user ' + username + ':' + password + '. Status code: ' + err.code + ' Error: ' + err.message);
          return done(err);
        });
      });
    }));

  app.use(session({
    secret: 'R2!r1$4Pp', // session secret
    resave: true,
    saveUninitialized: true
  }));
  app.use(passport.initialize());
  app.use(passport.session()); // persistent login sessions
  app.use(flash()); // use connect-flash for flash messages stored in session

  // LOGIN ===============================
  // show the login form
  app.get('/login', function (req, res) {
    // if (req.isAuthenticated()) {
    //   res.redirect(env.root);
    // } else {
    res.render('login', {
      message: arrayUtil.getFirst(req.flash('errorMessage')),
      username: arrayUtil.getFirst(req.flash('username')),
      appRoot: env.root
    });
    // }
  });

  app.get('/logout', function (req, res) {
    req.logout();
    res.redirect(env.root + 'login');
  });

  // process the login form
  app.post('/login', passport.authenticate('local-login', {
    successRedirect: env.root,
    failureRedirect: env.root + 'login', // redirect back to the login page if there is an error
    failureFlash: true // allow flash messages
  }));

  app.use(function (req, res, next) {
    var allowedStartPath = ['/img/', '/css/', '/lib/'];
    var isPathAllowed = function (path) {
      for (var i = 0; i < allowedStartPath.length; i++) {
        if (path.startsWith(allowedStartPath[i])) {
          return true;
        }
      }
      return false;
    };
    var isResource = function (path) {
      var resource = path.substring(path.lastIndexOf('/'));
      return resource.indexOf('.') != -1
    };
    var isApiCall = function (path) {
      return path.startsWith('/api/');
    };

    if (req.isAuthenticated() || isPathAllowed(req.path)) {
      return next();
    } else if (isResource(req.path) || isApiCall(req.path)) {
      res.sendStatus(401)
    } else {
      res.redirect(env.root + 'login');
    }
  });
};
