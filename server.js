#!/usr/bin/env node
/**
 * Module dependencies.
 */
var appConfig = require('./src/server/server-config');


appConfig.emitter.on('ready', function(env) {
  var app = require('./src/server/app');
  var logger = require('./src/server/utils/logger');
  var http = require('http');
  var fs = require('fs');
  fs.writeFile('./public/environment.js',
    "window.environment = " + JSON.stringify(env)
  )
  /**
   * Get port from environment and store in Express.
   */
  var port = normalizePort(process.env.PORT || env.port);
  app.set('port', port);

  /**
   * Create HTTP server.
   */

  //var server = http.createServer(app);

  /**
   * Listen on provided port, on all network interfaces.
   */

  var server = app.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);

  /**
   * Normalize a port into a number, string, or false.
   */

  function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
      // named pipe
      return val;
    }

    if (port >= 0) {
      // port number
      return port;
    }

    return false;
  }

  /**
   * Event listener for HTTP server "error" event.
   */

  function onError(error) {
    if (error.syscall !== 'listen') {
      throw error;
    }

    var bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }

  /**
   * Event listener for HTTP server "listening" event.
   */

  function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
      ? 'pipe ' + addr
      : 'port ' + addr.port;
    logger.info('Listening on ' + bind);
    console.log('Listening on ' + bind);
  }
});

appConfig.load();
